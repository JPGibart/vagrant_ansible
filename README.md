Dans ce repo, on trouve les fichiers Vagrant et Ansible pour le deploiement de l'infrastructure

Le fichier Vagrant_Master_Jenkins configure la machine master Jenkins

Le fichier Vagrant_Slave_Jenkins configure la machine slave Jenkins, accessible via le port 9000 en ssh.

Le fichier Vagrant_Server_Test configure la machine de test, accessible via le port 9100 en ssh.

Le fichier Vagrant_Server_Prod configure la machine de prod, accessible via le port 9200 en ssh.

